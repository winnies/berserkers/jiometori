#include <iostream>
#include "DGtal/base/Common.h"
#include "DGtal/io/readers/GenericReader.h"
#include "DGtal/images/ImageHelper.h"
#include "DGtal/images/Image.h"
#include "ConfigExamples.h"

#include "DGtal/helpers/StdDefs.h"

#include "DGtal/io/viewers/Viewer3D.h"
#include "DGtal/io/DrawWithDisplay3DModifier.h"
#include "DGtal/io/colormaps/HueShadeColorMap.h"
#include "DGtal/io/Color.h"

#include "DGtal/kernel/SpaceND.h"
#include "DGtal/kernel/domains/HyperRectDomain.h"
#include "DGtal/images/ImageSelector.h"

#include "DGtal/geometry/volumes/distance/DistanceTransformation.h"
#include "DGtal/images/SimpleThresholdForegroundPredicate.h"
#include "DGtal/helpers/StdDefs.h"

///////////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace DGtal;



struct hueFct{
 inline
 unsigned int operator() (unsigned int aVal) const
  {
    HueShadeColorMap<unsigned int>  hueShade(0,255);
    Color col = hueShade((unsigned int)aVal);
    return  (((unsigned int) col.red()) <<  16)| (((unsigned int) col.green()) << 8)|((unsigned int) col.blue());
  }
};

// Définition du type de viewer à utiliser. 
typedef Viewer3D<> ViewerType ;
// Définition du type de conteneur à utiliser pour l'image du premier exercice. 
typedef ImageContainerBySTLVector<Z3i::Domain,  float> Image3D;
// Définition du type de conteneur à utiliser pour l'image de la transformée en distance. 
typedef ImageSelector<Z3i::Domain, unsigned char>::Type Image;

// Parcours et traitement d'un volume. 
void imageSandbox(ViewerType& viewer, std::string filename);
// Tranformée en distance.
void transformeeEnDistance(ViewerType& viewer, std::string filename);

// Méthode pour générer des voxels de manière aléatoire. 
template<typename Image>
void randomSeeds(Image &image, const unsigned int nb, const int value);

///////////////////////////////////////////////////////////////////////////////

int main( int argc, char** argv )
{
  QApplication application(argc,argv);
  ViewerType viewer;
  
  // Appel aux méthodes des exercices.
  if (argc > 2)
  {
    std::stringstream ssAlgo;
    ssAlgo << std::string(argv[1]);

    std::stringstream ssFile;
    ssFile << std::string(argv[2]);
    if (ssAlgo.str() == "Sandbox")
      imageSandbox(viewer, ssFile.str());
    else if (ssAlgo.str() == "DT")
      transformeeEnDistance(viewer, ssFile.str());
  }
  else
  {
    std::cout << "Les paramètres n'a pas été fourni." << std::endl;
    std::cout << "startCode <paramètre = {Sandbox | DT}> <nom du fichier>" << std::endl;
    return 0;
  }
  
  return application.exec();
}


/**
 * Cette fonction vous permettra de commencer à pratiquer avec le chargement d'objets 
 * volumiques. Les parcourir, retrouver les valeurs affectées à chaque voxel et les 
 * les modifier. 
 * \param Visualisateur à utiliser.
 * \param Nom du fichier.
 *
 */
void imageSandbox(ViewerType& viewer, std::string filename)
{
  // Lance le visusalisateur. 
  viewer.show();
  
  //Chargement d'une image dans une structure de données ImageContainerBySTLVector.
  std::string inputFilename = examplesPath + "/" + filename;
  Image3D image = GenericReader<Image3D>::import(inputFilename);

  // Obtention du domaine (taille) de l'image chargée. 
  Z3i::Domain initialDomain = image.domain();
  Z3i::Point initialDomainCenter = (initialDomain.lowerBound() + initialDomain.upperBound()) / 2;

  std::cout << "lowerBound initialDomain :" << initialDomain.lowerBound() << std::endl;
  std::cout << "upperBound initialDomain :" << initialDomain.upperBound() << std::endl;
  std::cout << "center initialDomain :" << initialDomainCenter << std::endl;

  // Création d'un sous-domaine d'intérêt
  Z3i::Point pointInitial(220, 50, 10);
  Z3i::Point pointFinal(260, 100, 20);
  Z3i::Domain sousDomain(pointInitial, pointFinal);
  Z3i::Point sousDomainCenter = (sousDomain.lowerBound() + sousDomain.upperBound()) / 2;

  // Fonction calculant la distance euclidienne au carré
  // entre un point et le centre du domaine
  auto distanceEuclidienne = [&](Z3i::Point p)
  {
    return pow(initialDomainCenter[0] - p[0], 2)
            + pow(initialDomainCenter[1] - p[1], 2)
            + pow(initialDomainCenter[2] - p[2], 2);
  };

  // Pour centrer au niveau du sous domaine
//  auto distanceEuclidienne = [&](Z3i::Point p)
//  {
//    return pow(sousDomainCenter[0] - p[0], 2)
//            + pow(sousDomainCenter[1] - p[1], 2)
//            + pow(sousDomainCenter[2] - p[2], 2);
//  };

  float maxDist = 0;

  // Si on se base sur le sous-domaine
//  for(Z3i::Domain::ConstIterator it= sousDomain.begin(),
//    itend = sousDomain.end(); it != itend; ++it)
//  {

  // Première passe pour définir la range de couleurs.
  for(Z3i::Domain::ConstIterator it= initialDomain.begin(),
    itend = initialDomain.end(); it != itend; ++it)
  {
    float value = image(*it);
    if (value > 100.f && value < 150.f)
    {
        auto dist = distanceEuclidienne(*it);
        if(dist > maxDist) maxDist = dist;
    }
  }

  // Définition du gradient des couleurs.
  GradientColorMap<long> gradient(0, maxDist);
  gradient.addColor(Color::Red);
  gradient.addColor(Color::Yellow);
  gradient.addColor(Color::Green);
  gradient.addColor(Color::Cyan);
  gradient.addColor(Color::Blue);
  gradient.addColor(Color::Magenta);
  gradient.addColor(Color::Red);

  // Pour n'afficher que le sous-domaine
//  for(Z3i::Domain::ConstIterator it= sousDomain.begin(),
//    itend = sousDomain.end(); it != itend; ++it)
//  {

  for(Z3i::Domain::ConstIterator it= initialDomain.begin(),
    itend = initialDomain.end(); it != itend; ++it)
  { 
    float value = image(*it);

    // Pour tronquer selon la coordonnée x des voxels
    // if((*it)[0] < 150) continue;

    // Pour n'afficher que les voxels suppérieurs à 0
    // if(value > 0) {

    // Pour n'afficher que les voxels dont la valeur est comprise
    // entre 100 et 150
    if (value > 100.f && value < 150.f)
    {
        // Calcul de la transformée en distance pour le voxel courant.
        auto valDist = distanceEuclidienne(*it);

        // Calcul du gradient de couleur pour cette distance.
        Color c = gradient(valDist);
        viewer << CustomColors3D(Color((float)(c.red()),
                                       (float)(c.green()),
                                       (float)(c.blue(),205)),
                                 Color((float)(c.red()),
                                       (float)(c.green()),
                                       (float)(c.blue()),205));

        // Affichage du voxel
        viewer << *it;
    }
  }

  viewer << SetMode3D(image.className(), "BoundingBox");
  viewer << ViewerType::updateDisplay;
  
}

/**
 * Fonction de la transformée en distance à partir de quelques points germes.
 * La distance est calculée à partir de chaque point. Donc, la distance dans un 
 * voxel est la distance minimale à tous les points germes. 
 * \param le visualisateur à utiliser. 
 * \param Nom du fichier.
 *
 */
void transformeeEnDistance(ViewerType& viewer, std::string filename)
{
  // Affichage de la visualisation. 
  viewer.show();
  // Nombre du fichier à charger. 
  std::string inputFilename = examplesPath + "/" + filename;

  // Création du type d'image. 
  //Default image selector = STLVector
  typedef ImageSelector<Z3i::Domain, unsigned char>::Type Image;

  //Chargement du fichier image dans la structure. 
  Image image = VolReader<Image>::importVol( inputFilename );
  // Obtention du domaine (taille) de l'image. 
  Z3i::Domain domain = image.domain();

   Image imageSeeds(domain);
   // Avec les graines aléatoires
//   for ( Image::Iterator it = imageSeeds.begin(), itend = imageSeeds.end();it != itend; ++it)
//     (*it)=1;
//   //imageSeeds.setValue(p0, 0 );
//   randomSeeds(imageSeeds, 70, 0);

   // Distance au complémentaire (sans les graines aléatoires)
   for(Z3i::Domain::ConstIterator it= domain.begin(),
     itend = domain.end(); it != itend; ++it)
   {
        imageSeeds.setValue(*it, (image(*it) > 0) ? 1 : 0);
   }
    
  typedef functors::SimpleThresholdForegroundPredicate<Image> Predicate;
  Predicate aPredicate(imageSeeds,0);
  
  // Création de type et de l'objet pour appliquer la transformée. 
  typedef  DistanceTransformation<Z3i::Space,Predicate, Z3i::L2Metric> DTL2;
  DTL2 dtL2(&domain, &aPredicate, &Z3i::l2Metric);

  // Detection des distances minimales et maximales. 
  unsigned int min = 0;
  unsigned int max = 0;
  for(DTL2::ConstRange::ConstIterator it = dtL2.constRange().begin(),
	itend=dtL2.constRange().end();
      it!=itend;
      ++it)
  {
    if(  (*it) < min )
      min=(*it);
    if( (*it) > max )
      max=(*it);
  }
  
  //Spécification des gradients de couleur pour la visualisation.
  GradientColorMap<long> gradient( 0,30);
  gradient.addColor(Color::Red);
  gradient.addColor(Color::Yellow);
  gradient.addColor(Color::Green);
  gradient.addColor(Color::Cyan);
  gradient.addColor(Color::Blue);
  gradient.addColor(Color::Magenta);
  gradient.addColor(Color::Red);

  // Affectation du mode de visualisation 3D. 
  viewer << SetMode3D( (*(domain.begin())).className(), "Paving" );

  // Création d'un sous-domaine d'intérêt (coupe le modèle en 2 selon l'axe des x).
  Z3i::Point pointInitial(0, 0, 0);
  Z3i::Point pointFinal(domain.upperBound()[0] / 2, domain.upperBound()[1], domain.upperBound()[2]);
  Z3i::Domain sousDomain(pointInitial, pointFinal);

  // Parcours de tous les voxels de l'image avec un iterateur sur le domaine.
//  for(Z3i::Domain::ConstIterator it = domain.begin(), itend = domain.end();
//      it!=itend;
//      ++it)
//  {

  // Parcours de tous les voxels de l'image avec un iterateur sur le sous-domaine (domaine tronqué).
  for(Z3i::Domain::ConstIterator it = sousDomain.begin(), itend = sousDomain.end();
      it!=itend;
      ++it)
  {

    // Calcul de la transformée en distance pour le voxel courant. 
    double valDist= dtL2( (*it) );

    // Calcul du gradient de couleur pour cette distance.
    Color c= gradient(valDist);
    viewer << CustomColors3D(Color((float)(c.red()),
				   (float)(c.green()),
                   (float)(c.blue()),
                   205),
			     Color((float)(c.red()),
				   (float)(c.green()),
                   (float)(c.blue()),
                   205));

    // Le viewer reçoit le prochain voxel pour visualisation.
    if (image(*it) > 0)
      viewer << *it ;
  }
  
  //viewer << ClippingPlane(0,1,0, -40) << Viewer3D<>::updateDisplay;
  // Mise à jour du visualisateur après le parcours de tous le voxels. 
  viewer<< Viewer3D<>::updateDisplay;
}


/**
 * Cette fonction genère un ensemble de points afin de les placer 
 * dans le volume comme les germes de la transformée en distance. 
 * \param image.
 * \param nombre de germes.
 * \param value à affecter comme seuil. 
 *
 */ 
template<typename Image>
 void randomSeeds(Image &image, const unsigned int nb, const int value)
 {
   typename Image::Point p, low = image.domain().lowerBound();
   typename Image::Vector ext;
   srand ( time(NULL) );
 
   ext = image.extent();
 
   for (unsigned int k = 0 ; k < nb; k++)
     {
       for (unsigned int dim = 0; dim < Image::dimension; dim++)
         p[dim] = rand() % (ext[dim]) +  low[dim];
 
       image.setValue(p, value);
     }
 }
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
