/*
    Exemples de transformations en OpenCV, avec zoom, seuil et affichage en
    couleurs. L'image de niveau est en CV_32SC1.

    $ make ex01-transfos
    $ ./ex01-transfos [-mag width height] [-thr seuil] image_in [image_out]

    CC BY-SA Edouard.Thiel@univ-amu.fr - 07/09/2020

                        --------------------------------

    Renommez ce fichier tp<n°>-<vos-noms>.cpp
    Écrivez ci-dessous vos NOMS Prénoms et la date de la version :

    Escales Loïc - Galland Thomas - version du 06/11/2020
*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <opencv2/opencv.hpp>
#include "gd-util.hpp"

// Opérateur manquant OpenCV
namespace cv {
    bool operator<(const cv::Vec3b& v1, const cv::Vec3b& v2)
    {
        return cv::norm(v1) < cv::norm(v2);
    }
}

// Retourne vrai si les coordonnées correspondent à un pixel de l'image (matrice)
bool is_valid(const cv::Mat& img, int row, int col) {
    return 0 <= row && row < img.rows && 0 <= col && col < img.cols;
}

enum NumeroElem {X, VERT, HOR, LAST};

// Element structurant
struct ElemStruct
{
    std::vector<cv::Point2i> points;

    ElemStruct(NumeroElem ne)
    {
        switch (ne)
        {
        case X:
            points = {{0, 0}, {0, -1}, {1, 0}, {0, 1}, {-1, 0}};
            break;
        case VERT:
            points = {{0, -2}, {0, -1}, {0, 0}, {0, 1}, {0, 2}};
            break;
        case HOR:
            points = {{-2, 0}, {-1, 0}, {0, 0}, {1, 0}, {2, 0}};
            break;
        default :
            assert(false);
        }
    }

    void for_each(const cv::Mat& img_niv, int x, int y, const std::function<void(int, int)>& op) const
    {
        for(const cv::Point2i& p : points)
        {
            int nx = x + p.x;
            int ny = y + p.y;
            if(!is_valid(img_niv, ny, nx)) continue;
            op(nx, ny);
        }
    }
};

ElemStruct* ELEM = nullptr;
int FILTRE_MAX_LOCAL = 0;

//----------------------------------- M Y -------------------------------------

class My {
  public:
    cv::Mat img_src, img_res1, img_res2, img_niv, img_coul;
    Loupe loupe;
    int seuil = 127;
    int clic_x = 0;
    int clic_y = 0;
    int clic_n = 0;

    enum Recalc { R_RIEN, R_LOUPE, R_TRANSFOS, R_SEUIL };
    Recalc recalc = R_SEUIL;

    NumeroElem numero_elem = X;
    ElemStruct elem = ElemStruct(X);

    void reset_recalc ()             { recalc = R_RIEN; }
    void set_recalc   (Recalc level) { if (level > recalc) recalc = level; }
    int  need_recalc  (Recalc level) { return level <= recalc; }

    // Rajoutez ici des codes A_TRANSx pour le calcul et l'affichage
    enum Affi { A_ORIG, A_SEUIL, A_TRANS1, A_TRANS2, A_TRANS3, A_TRANS4, A_TRANS5, A_TRANS6};
    Affi affi = A_ORIG;
};

//----------------------- T R A N S F O R M A T I O N S -----------------------

// Placez ici vos fonctions de transformations à la place de ces exemples

// Dilate les formes de l'image selon l'élément structurant
void dilatation(cv::Mat img_niv, const ElemStruct& elem)
{
    cv::Mat img_copy = img_niv.clone();
    for (int y = 0; y < img_copy.rows; y++)
    for (int x = 0; x < img_copy.cols; x++)
    {
        cv::Vec3b max = {0, 0, 0};
        elem.for_each(img_niv, x, y, [&](int vx, int vy){
            max = std::max(img_copy.at<cv::Vec3b>(vy, vx), max);
        });
        img_niv.at<cv::Vec3b>(y, x) = max;
    }
}

// Erode les formes de l'image selon l'élément structurant
void erosion(cv::Mat img_niv, const ElemStruct& elem)
{
    cv::Mat img_copy = img_niv.clone();
    for (int y = 0; y < img_copy.rows; y++)
    for (int x = 0; x < img_copy.cols; x++)
    {
        cv::Vec3b min = {255, 255, 255};
        elem.for_each(img_niv, x, y, [&](int vx, int vy){
            min = std::min(img_copy.at<cv::Vec3b>(vy, vx), min);
        });
        img_niv.at<cv::Vec3b>(y, x) = min;
    }
}

// Dilatation -> Erosion
void fermeture(cv::Mat img_niv, const ElemStruct& elem)
{
    dilatation(img_niv, elem);
    erosion(img_niv, elem);
}

// Erosion -> Dilatation
void ouverture(cv::Mat img_niv, const ElemStruct& elem)
{
    erosion(img_niv, elem);
    dilatation(img_niv, elem);
}

// Gradiant morphologique (mise en valeur des contours)
void gradient(cv::Mat img_niv, const ElemStruct& elem)
{
    cv::Mat img_dilat = img_niv.clone();
    cv::Mat img_ero = img_niv.clone();

    dilatation(img_dilat, elem);
    erosion(img_ero, elem);

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        img_niv.at<cv::Vec3b>(y, x) = img_dilat.at<cv::Vec3b>(y, x) - img_ero.at<cv::Vec3b>(y, x);
    }
}

struct Pixel {
    int x, y;

    bool operator==(const Pixel& p) const {
        return x == p.x && y == p.y;
    }
};

int& at(cv::Mat& img, const Pixel& p) {
    return img.at<int>(p.y, p.x);
}

// Applique une fonction (void(int,int)) au niveau de chaque pixel voisin dans le 8-voisinage
void foreach_c8(const cv::Mat& img, const Pixel& p, const std::function<void(const Pixel&)>& op) {
    for (int i = -1; i < 2; ++i) {
        for (int j = -1; j < 2; ++j) {
            Pixel n; n.y = p.y + i; n.x = p.x + j;
            if ((n.y == p.y && n.x == p.x) ||
                !is_valid(img, n.y, n.x)) continue;
            op(n);
        }
    }
}

void fifo_add(const Pixel& p, std::queue<Pixel>& queue) {
    queue.push(p);
}

Pixel fifo_remove(std::queue<Pixel>& queue) {
    auto p = queue.front();
    queue.pop();
    return p;
}

void watershed_by_immersion(cv::Mat im_src) {
   cv::Mat im(im_src.rows, im_src.cols, CV_32SC1);
   for (int y = 0; y < im.rows; y++)
   for (int x = 0; x < im.cols; x++) {
       im.at<int>(y, x) = im_src.at<cv::Vec3b>(y, x)[0];
   }

   cv::Mat lab = im.clone();
   cv::Mat dist = im.clone();
   const int INIT = -1, MASK = -2, WSHED = 0;
   const Pixel FICTITIOUS {-1, -1};

   std::vector<Pixel> D;
   D.reserve(im.rows * im.cols);
   for (int y = 0; y < im.rows; y++)
   for (int x = 0; x < im.cols; x++) {
       D.push_back(Pixel{x, y});
   }

   int curlab = 0;
   std::queue<Pixel> queue;
   for (auto& p : D) {
       at(lab, p) = INIT;
       at(dist, p) = 0;
   }

   std::sort(D.begin(), D.end(), [&](const Pixel& p1, const Pixel& p2) {
       return at(im, p1) < at(im, p2);
   });

   // start flooding

   int hmin = at(im, D.front()), hmax = at(im, D.back());
   for (int h = hmin; h <= hmax; ++h) {
       for (auto& p : D) {
           if (at(im, p) != h) continue;

           at(lab, p) = MASK;

           bool has_q = false;
           foreach_c8(im, p, [&](const Pixel& q) { has_q = has_q || at(lab, q) > 0 || at(lab, q) == WSHED; });
           if (has_q) {
               at(dist, p) = 1;
               fifo_add(p, queue);
           }
       }

       int curdist = 1;
       fifo_add(FICTITIOUS, queue);

       while (true) {
           auto p = fifo_remove(queue);

           if (p == FICTITIOUS) {
                if (queue.empty()) break;
                else {
                    fifo_add(FICTITIOUS, queue);
                    curdist = curdist + 1;
                    p = fifo_remove(queue);
                }
           }

           foreach_c8(im, p, [&](const Pixel& q)
           {
               if (at(dist, q) < curdist && (at(lab, q) > 0 || at(lab, q) == WSHED)) {
                   if (at(lab, q) > 0) {
                       if (at(lab, p) == MASK || at(lab, p) == WSHED) {
                           at(lab, p) = at(lab, q);
                       } else if (at(lab, p) != at(lab, q)) {
                           at(lab, p) = WSHED;
                       }
                   } else if (at(lab, p) == MASK) {
                       at(lab, p) = WSHED;
                   }
               } else if (at(lab, q) == MASK && at(dist, q) == 0) {
                   at(dist, q) = curdist + 1;
                   fifo_add(q, queue);
               }
           });
       }

       // detect and process new minima at level h

       for (auto& p : D) {
           if (at(im, p) != h) continue;

           at(dist, p) = 0;
           if (at(lab, p) == MASK) {
               curlab = curlab + 1;
               fifo_add(p, queue);
               at(lab, p) = curlab;

               while (!queue.empty()) {
                   auto q = fifo_remove(queue);

                   foreach_c8(im, q, [&](const Pixel& r)
                   {
                       if (at(lab, r) == MASK) {
                           fifo_add(r, queue);
                           at(lab, r) = curlab;
                       }
                   });
               }
           }
       }
   }

   cv::Mat lab_eq(im_src.rows, im_src.cols, CV_8UC1);
   for (int y = 0; y < im.rows; y++)
   for (int x = 0; x < im.cols; x++) {
       lab_eq.at<unsigned char>(y, x) = lab.at<int>(y, x);
   }
   cv::equalizeHist(lab_eq, lab_eq);

   for (int y = 0; y < im.rows; y++)
   for (int x = 0; x < im.cols; x++) {
       auto& p = im_src.at<cv::Vec3b>(y, x);
        p[0] =
        p[1] =
        p[2] = lab_eq.at<unsigned char>(y, x);
   }
}

// Appelez ici vos transformations selon affi
void effectuer_transformations (My::Affi affi, cv::Mat img_niv)
{
    switch (affi) {
        case My::A_TRANS1 :
            dilatation(img_niv, *ELEM);
            break;
        case My::A_TRANS2 :
            erosion(img_niv, *ELEM);
            break;
        case My::A_TRANS3 :
            fermeture(img_niv, *ELEM);
            break;
        case My::A_TRANS4:
            ouverture(img_niv, *ELEM);
            break;
        case My::A_TRANS5:
            gradient(img_niv, *ELEM);
            break;
        case My::A_TRANS6:
            watershed_by_immersion(img_niv);
            break;
        default : ;
    }
}

//---------------------------- C A L L B A C K S ------------------------------

// Callback des sliders
void onZoomSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->loupe.reborner (my->img_res1, my->img_res2);
    my->set_recalc(My::R_LOUPE);
}

void onSeuilSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->set_recalc(My::R_SEUIL);
}

// Callback pour la souris
void onMouseEventSrc (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->clic_x = x;
            my->clic_y = y;
            my->clic_n = 1;
            break;
        case cv::EVENT_MOUSEMOVE :
            // std::cout << "mouse move " << x << "," << y << std::endl;
            if (my->clic_n == 1) {
                my->loupe.deplacer (my->img_res1, my->img_res2,
                    x - my->clic_x, y - my->clic_y);
                my->clic_x = x;
                my->clic_y = y;
                my->set_recalc(My::R_LOUPE);
            }
            break;
        case cv::EVENT_LBUTTONUP :
            my->clic_n = 0;
            break;
    }
}

void onMouseEventLoupe (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->loupe.afficher_tableau_valeurs (my->img_niv, x, y, 5, 4);
            break;
    }
}

void afficher_aide() {
    // Indiquez les transformations ici
    std::cout <<
        "Touches du clavier:\n"
        "   a    affiche cette aide\n"
        " hHlL   change la taille de la loupe\n"
        "   i    inverse les couleurs de src\n"
        "   o    affiche l'image src originale\n"
        "   s    affiche l'image src seuillée\n"
        "   1    dilatation\n"
        "   2    érosion\n"
        "   3    fermeture\n"
        "   4    ouverture\n"
        "   5    gradient morphologique\n"
        "   6    watershed par immersion\n"
        "  esc   quitte\n"
    << std::endl;
}

// Callback "maison" pour le clavier
int onKeyPressEvent (int key, void *data)
{
    My *my = (My*) data;

    if (key < 0) return 0;        // aucune touche pressée
    key &= 255;                   // pour comparer avec un char
    if (key == 27) return -1;     // ESC pour quitter

    ELEM = &my->elem;

    switch (key) {
        case 'a' :
            afficher_aide();
            break;
        case 'h' :
        case 'H' :
        case 'l' :
        case 'L' : {
            std::cout << "Taille loupe" << std::endl;
            int h = my->img_res2.rows, w = my->img_res2.cols;
            if      (key == 'h') h = h >=  200+100 ? h-100 :  200;
            else if (key == 'H') h = h <= 2000-100 ? h+100 : 2000;
            else if (key == 'l') w = w >=  200+100 ? w-100 :  200;
            else if (key == 'L') w = w <= 2000-100 ? w+100 : 2000;
            my->img_res2 = cv::Mat(h, w, CV_8UC3);
            my->loupe.reborner(my->img_res1, my->img_res2);
            my->set_recalc(My::R_LOUPE);
          } break;
        case 'i' :
            std::cout << "Couleurs inversées" << std::endl;
            inverser_couleurs(my->img_src);
            my->set_recalc(My::R_SEUIL);
            break;
        case 'o' :
            std::cout << "Image originale" << std::endl;
            my->affi = My::A_ORIG;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case 's' :
            std::cout << "Image seuillée" << std::endl;
            my->affi = My::A_SEUIL;
            my->set_recalc(My::R_SEUIL);
            break;
        // Rajoutez ici des touches pour les transformations.
        // Dans my->set_recalc, passez :
        //   My::R_SEUIL pour faire le calcul à partir de l'image originale seuillée
        //   My::R_TRANSFOS pour faire le calcul à partir de l'image actuelle
        case '1' :
            std::cout << "Dilatation" << std::endl;
            my->affi = My::A_TRANS1;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case '2' :
            std::cout << "Erosion" << std::endl;
            my->affi = My::A_TRANS2;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case '3' :
            std::cout << "Fermeture" << std::endl;
            my->affi = My::A_TRANS3;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case '4':
            std::cout << "Ouverture" << std::endl;
            my->affi = My::A_TRANS4;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case '5':
            std::cout << "Gradiant morphologique" << std::endl;
            my->affi = My::A_TRANS5;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case '6':
            std::cout << "Watershed par immersion" << std::endl;
            my->affi = My::A_TRANS6;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case 'd':
            my->numero_elem = static_cast<NumeroElem>(static_cast<int>(my->numero_elem)+1);
            if (my->numero_elem == LAST) {
                my->numero_elem = X;
            }
            my->elem = ElemStruct(my->numero_elem);
            std::cout << "Elem courant : " << my->numero_elem << std::endl;
            my->set_recalc(My::R_SEUIL);
            break;
        default :
            //std::cout << "Touche '" << char(key) << "'" << std::endl;
            break;
    }
    return 1;
}

//---------------------------------- M A I N ----------------------------------

void afficher_usage (char *nom_prog) {
    std::cout << "Usage: " << nom_prog
              << "[-mag width height] [-thr seuil] in1 [out2]"
              << std::endl;
}

int main (int argc, char**argv)
{
    My my;
    char *nom_in1, *nom_out2, *nom_prog = argv[0];
    int zoom_w = 600, zoom_h = 500;

    while (argc-1 > 0) {
        if (!strcmp(argv[1], "-mag")) {
            if (argc-1 < 3) { afficher_usage(nom_prog); return 1; }
            zoom_w = atoi(argv[2]);
            zoom_h = atoi(argv[3]);
            argc -= 3; argv += 3;
        } else if (!strcmp(argv[1], "-thr")) {
            if (argc-1 < 2) { afficher_usage(nom_prog); return 1; }
            my.seuil = atoi(argv[2]);
            argc -= 2; argv += 2;
        } else break;
    }
    if (argc-1 < 1 or argc-1 > 2) { afficher_usage(nom_prog); return 1; }
    nom_in1  = argv[1];
    nom_out2 = (argc-1 == 2) ? argv[2] : NULL;

    // Lecture image
    my.img_src = cv::imread (nom_in1, cv::IMREAD_COLOR);  // produit du 8UC3
    if (my.img_src.empty()) {
        std::cout << "Erreur de lecture" << std::endl;
        return 1;
    }

    // Création résultats
    my.img_res1 = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.img_res2 = cv::Mat(zoom_h, zoom_w, CV_8UC3);
    my.img_niv  = cv::Mat(my.img_src.rows, my.img_src.cols, CV_32SC1);
    my.img_coul = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.loupe.reborner(my.img_res1, my.img_res2);

    // Création fenêtre
    cv::namedWindow ("ImageSrc", cv::WINDOW_AUTOSIZE);
    cv::createTrackbar ("Zoom", "ImageSrc", &my.loupe.zoom, my.loupe.zoom_max,
        onZoomSlide, &my);
    cv::createTrackbar ("Seuil", "ImageSrc", &my.seuil, 255,
        onSeuilSlide, &my);
    cv::createTrackbar ("Filtre", "ImageSrc", &FILTRE_MAX_LOCAL, 500,
        onSeuilSlide, &my);
    cv::setMouseCallback ("ImageSrc", onMouseEventSrc, &my);

    cv::namedWindow ("Loupe", cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback ("Loupe", onMouseEventLoupe, &my);

    afficher_aide();

    // Boucle d'événements
    for (;;) {

        if (my.need_recalc(My::R_SEUIL))
        {
            // std::cout << "Calcul seuil" << std::endl;
            cv::Mat img_gry;
            cv::cvtColor (my.img_src, img_gry, cv::COLOR_BGR2GRAY);
            cv::threshold (img_gry, img_gry, my.seuil, 255, cv::THRESH_BINARY);
            img_gry.convertTo (my.img_niv, CV_32SC1,1., 0.);
        }

        if (my.need_recalc(My::R_TRANSFOS))
        {
            // std::cout << "Calcul transfos" << std::endl;
            if (my.affi != My::A_ORIG) {
                effectuer_transformations (my.affi, my.img_coul);
//                 effectuer_transformations (my.affi, my.img_niv);
//                 representer_en_couleurs_vga (my.img_niv, my.img_coul);
            } else my.img_coul = my.img_src.clone();
        }

        if (my.need_recalc(My::R_LOUPE)) {
            // std::cout << "Calcul loupe puis affichage" << std::endl;
            my.loupe.dessiner_rect    (my.img_coul, my.img_res1);
            my.loupe.dessiner_portion (my.img_coul, my.img_res2);
            cv::imshow ("ImageSrc", my.img_res1);
            cv::imshow ("Loupe"   , my.img_res2);
        }
        my.reset_recalc();

        // Attente du prochain événement sur toutes les fenêtres, avec un
        // timeout de 15ms pour détecter les changements de flags
        int key = cv::waitKey (15);

        // Gestion des événements clavier avec une callback "maison" que l'on
        // appelle nous-même. Les Callbacks souris et slider sont directement
        // appelées par waitKey lors de l'attente.
        if (onKeyPressEvent (key, &my) < 0) break;
    }

    // Enregistrement résultat
    if (nom_out2) {
        if (! cv::imwrite (nom_out2, my.img_coul))
             std::cout << "Erreur d'enregistrement" << std::endl;
        else std::cout << "Enregistrement effectué" << std::endl;
     }
    return 0;
}
