/*
    Exemples de transformations en OpenCV, avec zoom, seuil et affichage en
    couleurs. L'image de niveau est en CV_32SC1.

    $ make ex01-transfos
    $ ./ex01-transfos [-mag width height] [-thr seuil] image_in [image_out]

    CC BY-SA Edouard.Thiel@univ-amu.fr - 07/09/2020

                        --------------------------------

    Renommez ce fichier tp<n°>-<vos-noms>.cpp 
    Écrivez ci-dessous vos NOMS Prénoms et la date de la version :

    ESCALES Loïc et GALLAND Thomas - version du 11/10/2020
*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <functional>
#include <climits>
#include <opencv2/opencv.hpp>
#include "gd-util.hpp"


//----------------------------------- M Y -------------------------------------

class My {
  public:
    cv::Mat img_src, img_res1, img_res2, img_niv, img_coul;
    Loupe loupe;
    int seuil = 127;
    int clic_x = 0;
    int clic_y = 0;
    int clic_n = 0;

    enum Recalc { R_RIEN, R_LOUPE, R_TRANSFOS, R_SEUIL };
    Recalc recalc = R_SEUIL;

    void reset_recalc ()             { recalc = R_RIEN; }
    void set_recalc   (Recalc level) { if (level > recalc) recalc = level; }
    int  need_recalc  (Recalc level) { return level <= recalc; }

    // Rajoutez ici des codes A_TRANSx pour le calcul et l'affichage
    enum Affi { A_ORIG, A_SEUIL, A_TRANS1, A_TRANS2, A_TRANS3, A_TRANS4, A_TRANS5, A_TRANS6, A_TRANS7, A_TRANS8, A_TRANS9};
    Affi affi = A_ORIG;
};

//--------------------------------- OUTILS -----------------------------------

class Vector2f
{
private:
    std::array<float, 2> coords;

public:
    Vector2f();
    Vector2f(float x, float y);
    Vector2f(const float*);

    bool operator== (const Vector2f &) const;
    bool operator!= (const Vector2f &) const;

    Vector2f& operator+= (const Vector2f &);
    Vector2f& operator-= (const Vector2f &);
    Vector2f& operator*= (float);

    Vector2f operator+ (const Vector2f&) const;
    Vector2f operator- (const Vector2f&) const;
    Vector2f operator* (float) const;

    float operator[](size_t) const;
    float& operator[](size_t);

    friend std::ostream& operator<<(std::ostream&, const Vector2f&);
};

Vector2f::Vector2f() : coords{0.f, 0.f} {}

Vector2f::Vector2f(float x, float y) : coords{x, y} {}
Vector2f::Vector2f(const float* d) : coords{d[0], d[1]} {}

bool Vector2f::operator==(const Vector2f &p) const
{
    return coords == p.coords;
}

bool Vector2f::operator!=(const Vector2f &p) const
{
    return !(*this == p);
}

Vector2f& Vector2f::operator+=(const Vector2f &p)
{
    for (size_t i = 0; i < coords.size(); ++i) {
        coords[i] += p.coords[i];
    }

    return *this;
}

Vector2f& Vector2f::operator-=(const Vector2f &p)
{
    for (size_t i = 0; i < coords.size(); ++i) {
        coords[i] -= p.coords[i];
    }

    return *this;
}

Vector2f& Vector2f::operator*=(float f)
{
    for (size_t i = 0; i < coords.size(); ++i) {
        coords[i] *= f;
    }

    return *this;
}

Vector2f Vector2f::operator+(const Vector2f &p) const
{
    return Vector2f{*this} += p;
}

Vector2f Vector2f::operator-(const Vector2f &p) const
{
    return Vector2f{*this} -= p;
}

Vector2f Vector2f::operator*(float f) const
{
    return Vector2f{*this} *= f;
}

float Vector2f::operator[](size_t i) const
{
    return coords[i];
}

float &Vector2f::operator[](size_t i)
{
    return coords[i];
}

std::ostream& operator<<(std::ostream& out, const Vector2f& p)
{
    return out << "[ " << p.coords[0] << " " << p.coords[1] << " ]"; // can access private member Y::data
}

float cross(const Vector2f& a, const Vector2f& b) {
    return a[0] * b[1] - a[1] * b[0];
}

float dot(const Vector2f& v1, const Vector2f& v2) {
    return v1[0] * v2[0] + v1[1] * v2[1];
}

float length_squared(const Vector2f& v1, const Vector2f& v2) {
    return std::pow(v2[0] - v1[0], 2) + std::pow(v2[1] - v1[1], 2);
}

float length(const Vector2f& v1, const Vector2f& v2) {
    return std::sqrt(length_squared(v1, v2));
}

// Distance point - segment
float minimum_distance(const Vector2f& v, const Vector2f& w, const Vector2f& p) {
  const float l2 = length_squared(v, w);
  if (l2 == 0.0) return length(p, v);
  const float t = std::max(0.f, std::min(1.f, dot(p - v, w - v) / l2));
  const Vector2f projection = (w - v) * t + v;
  return length(p, projection);
}


//----------------------- T R A N S F O R M A T I O N S -----------------------

int seuil_Polyg;
bool connexite = false; // false == 4, true == 8

// Placez ici vos fonctions de transformations à la place de ces exemples

// Retourne vrai si les coordonnées correspondent à un pixel de l'image (matrice)
bool is_valid(const cv::Mat& img, int row, int col) {
    return 0 <= row && row < img.rows && 0 <= col && col < img.cols;
}

// Applique une fonction (void(int,int)) au niveau de chaque pixel voisin dans le 4-voisinage
void foreach_c4(int row, int col, const std::function<void(int,int)>& op) {
    static const int coords[4][2] = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};
    for (int const* coord : coords) {
        int r = row + coord[0], c = col + coord[1];
        op(r, c);
    }
}

// Applique une fonction (void(int,int)) au niveau de chaque pixel voisin dans le 8-voisinage
void foreach_c8(int row, int col, const std::function<void(int,int)>& op) {
    for (int i = -1; i < 2; ++i) {
        for (int j = -1; j < 2; ++j) {
            int r = row + i, c = col + j;
            if (r == row && c == col) continue;
            op(r, c);
        }
    }
}

// Marque les contours de forme (8-voisinage)
void marquer_contours_c8 (cv::Mat img_niv)
{
    CHECK_MAT_TYPE(img_niv, CV_32SC1)

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int &g = img_niv.at<int>(y,x);
        if (g == 255) {
            foreach_c4(y, x, [&](int row, int col){
                if(!is_valid(img_niv, row, col) || img_niv.at<int>(row, col) == 0) {
                    g = 1;
                }
            });
        }
    }
}

// Marque les contours de forme (4-voisinage)
void marquer_contours_c4 (cv::Mat img_niv)
{
    CHECK_MAT_TYPE(img_niv, CV_32SC1)

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int &g = img_niv.at<int>(y,x);
        if (g == 255) {
            foreach_c8(y, x, [&](int row, int col) {
                if(!is_valid(img_niv, row, col) || img_niv.at<int>(row, col) == 0) {
                    g = 1;
                }
            });
        }
    }
}

// Marque les contours de forme d'une couleur différente pour chaque entité (8-voisinage)
void numeroter_contours_c8 (cv::Mat img_niv)
{
    CHECK_MAT_TYPE(img_niv, CV_32SC1)

    cv::Mat labels;
    labels.create(img_niv.rows, img_niv.cols, CV_32SC1);
    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
        labels.at<int>(y,x) = -1;

    int nouveau_label = 1;

    auto get_nouveau_label = [&](){
        nouveau_label += 1;
        if (nouveau_label == 255) {
            nouveau_label += 1;
        }
        return nouveau_label;
    };

    std::vector<std::set<int>> groupes_eq;

    // Si on trouve dans le nouveau groupe d'équivalence un label déjà présent
    // dans un ancien groupe d'équivalence, on fusionne les groupes
    auto ajout_groupe_eq = [&](std::set<int> nouveau_groupe) {
        groupes_eq.erase(std::remove_if(groupes_eq.begin(), groupes_eq.end(), [&](const std::set<int>& groupe) {
            for (int label : groupe) {
                if(nouveau_groupe.find(label) != nouveau_groupe.end()) {
                    nouveau_groupe.insert(groupe.begin(), groupe.end());
                    return true;
                }
            }
            return false;
        }), groupes_eq.end());
        groupes_eq.push_back(nouveau_groupe);
    };

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int g = img_niv.at<int>(y,x);

        bool est_contour = false;
        if (g == 255) {
            foreach_c4(y, x, [&](int row, int col) {
                if(!is_valid(img_niv, row, col) || img_niv.at<int>(row, col) == 0) {
                    est_contour = true;
                }
            });
        }
        if (!est_contour) continue;

        std::set<int> labels_voisins;
        foreach_c8(y, x, [&](int row, int col) {
            if(!is_valid(img_niv, row, col)) return;
            int label = labels.at<int>(row, col);
            if(label != -1) {
                labels_voisins.insert(label);
            }
        });

        switch (labels_voisins.size()) {
            case 0: {
                int l = get_nouveau_label();
                ajout_groupe_eq({l});
                labels.at<int>(y,x) = l;
                break; }
            case 1:
                labels.at<int>(y,x) = *labels_voisins.begin();
                break;
            default:
                labels.at<int>(y,x) = *labels_voisins.begin();
                ajout_groupe_eq(labels_voisins);
                break;
        }
    }

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {   
        int label = labels.at<int>(y,x);
        if(label == -1) continue;

        for(size_t i = 0; i < groupes_eq.size(); ++i) {
            if(groupes_eq[i].find(label) != groupes_eq[i].end()) {
                img_niv.at<int>(y, x) = *groupes_eq[i].begin();
                break;
            }
        }
    }
}

// Contour caractérisé par un point de départ et une chaîne de Freeman
struct ContourF8 {
    int x, y;
    std::vector<int> freeman_string;
    int dir;
};

// Chaine de Freeman + direction du fond de départ
struct FreemanAndDir
{
    std::vector<int> freeman_string;
    int dir;
};

// Effectue le suivi d'un contour et retourne la chaîne de Freeman correspondante
FreemanAndDir suivre_contour_c8 (cv::Mat img_niv, int xA, int yA, int dirA, int num_contour)
{
    int nx8[] = {1, 1, 0, -1, -1, -1, 0, 1};
    int ny8[] = {0, 1, 1, 1, 0, -1, -1, -1};
    int dir_finale = -1;

    FreemanAndDir result;
    result.dir = dirA;

    for(int i = 0; i < 8; i++)
    {
        int d = (dirA+i) % 8;
        int x = xA + nx8[d];
        int y = yA + ny8[d];
        if(is_valid(img_niv, y, x) && img_niv.at<int>(y, x) > 0)
        {
            dir_finale = (d+4) % 8;
            break;
        }
    }

    int x = xA;
    int y = yA;
    int dir = dir_finale;

    do {

        img_niv.at<int>(y, x) = num_contour;
        dir = (dir + 4 - 1) % 8;
        int i;
        for(i = 0; i < 8; i++)
        {
            int d = (dir + 8 - i) % 8;
            int ny = y + ny8[d];
            int nx = x + nx8[d];
            if(!is_valid(img_niv, ny, nx)) continue;
            if(img_niv.at<int>(ny, nx) > 0)
            {
                x = nx;
                y = ny;
                dir = d;
                break;
            }
        }
        if(i == 8) return {};
        result.freeman_string.push_back(dir);
    } while( !(x == xA && y == yA && dir == dir_finale) );
    return result;
}

// Cherche le premier point de chaque contour et applique suivre_contour_c8
std::vector<ContourF8> effectuer_suivi_contours_c8 (cv::Mat img_niv)
{
    std::vector<ContourF8> contours;

    int num_contour = 1;
    for(int y = 0; y < img_niv.rows; y++)
    {
        for(int x = 0; x < img_niv.cols; x++)
        {
            if(img_niv.at<int>(y,x) != 255) continue;

            int dir = -1;
            if(x == img_niv.cols-1 || img_niv.at<int>(y, x+1) == 0) dir = 0;
            else if(y == img_niv.rows-1 || img_niv.at<int>(y+1, x) == 0) dir = 2;
            else if(x == 0 || img_niv.at<int>(y, x-1) == 0) dir = 4;
            else if(y == 0 || img_niv.at<int>(y-1, x) == 0) dir = 6;
//            if(y == 0 || img_niv.at<int>(y-1, x) == 0) dir = 6;
//            else if(x == 0 || img_niv.at<int>(y, x-1) == 0) dir = 4;
//            else if(y == img_niv.rows-1 || img_niv.at<int>(y+1, x) == 0) dir = 2;
//            else if(x == img_niv.cols-1 || img_niv.at<int>(y, x+1) == 0) dir = 0;

            if(dir >= 0)
            {
                ContourF8 c;
                c.x = x;
                c.y = y;
                FreemanAndDir freemanAndDir = suivre_contour_c8 (img_niv, x, y, dir, num_contour++);
                c.freeman_string = freemanAndDir.freeman_string;
                c.dir = freemanAndDir.dir;
                if(num_contour == 255) num_contour++;
                if (!c.freeman_string.empty()) {
                    contours.push_back(c);
                }
            }
        }
    }

    return contours;
}

// Contour dont les points sont marqués par des drapeaux
// en fonction de leur appartenance à l'approximation polygonale
struct ContourPol {
    struct Point {
        int x, y;
        bool sommet;
    };
    std::vector<Point> points;
};

// Approximation polygonale d'un contour (en fonction d'un certain seuil)
ContourPol approximer_contour_c8(const ContourF8& contourF8, float seuil) {
    ContourPol contour_pol;

    int nx8[] = {1, 1, 0, -1, -1, -1, 0, 1};
    int ny8[] = {0, 1, 1, 1, 0, -1, -1, -1};

    int x = contourF8.x, y = contourF8.y;
    contour_pol.points.push_back({x, y, true});
    for (int dir : contourF8.freeman_string) {
        x += nx8[dir];
        y += ny8[dir];
        contour_pol.points.push_back({x, y, true});
    }

    Vector2f A(contourF8.x, contourF8.y);
    float dist = 0;
    size_t B_i;
    for (size_t i = 0; i < contour_pol.points.size(); ++i) {
        const auto& p = contour_pol.points[i];
        Vector2f P(p.x, p.y);
        float new_dist = length_squared(A, P);
        if (dist < new_dist) {
            dist = new_dist;
            B_i = i;
        }
    }

    std::function<void(size_t, size_t)> recursive_func;
    recursive_func = [&](size_t p, size_t q) {
        if (q-p <= 2) {
            return;
        }

        Vector2f v(contour_pol.points[p].x, contour_pol.points[p].y), w(contour_pol.points[q].x, contour_pol.points[q].y);
        size_t r;
        float dist = 0;
        for (size_t i = p+1; i < q; ++i) {
            const auto& p = contour_pol.points[i];
            float new_dist = minimum_distance(v, w, Vector2f(p.x, p.y));
            if (dist < new_dist) {
                dist = new_dist;
                r = i;
            }
        }

        if (dist <= seuil) {
            for (size_t k = p+1; k < q; ++k) {
                contour_pol.points[k].sommet = false;
            }
        } else {
            recursive_func(p, r);
            recursive_func(r, q);
        }
    };

    recursive_func(0, B_i);
    recursive_func(B_i, contour_pol.points.size()-1);

    std::function<void()> relaxation;
    relaxation = [&]() {
        size_t f = 0, g, h;
        while(true) {
            do {
                if (f == contour_pol.points.size()) return;
                ++f;
            } while(!contour_pol.points[f].sommet);
            g = f+1;
            do {
                if (g == contour_pol.points.size()) return;
                ++g;
            } while(!contour_pol.points[g].sommet);
            h = g+1;
            do {
                if (h == contour_pol.points.size()) return;
                ++h;
            } while(!contour_pol.points[h].sommet) ;

            const auto& F = contour_pol.points[f];
            auto& G = contour_pol.points[g];
            const auto& H = contour_pol.points[h];

            if (minimum_distance(Vector2f(F.x, F.y), Vector2f(H.x, H.y), Vector2f(G.x, G.y)) <= seuil) {
                G.sommet = false;
            }

            f=g;
        }
    };

    relaxation();

    return contour_pol;
}

// Remplis avec une couleur le polygone passé en paramètre
void remplir_poly(ContourPol contour, cv::Mat img_niv, int coul)
{
    std::vector<cv::Point> contourPoints;
    for(ContourPol::Point& point : contour.points)
    {
        if(point.sommet) {
            contourPoints.push_back(cv::Point(point.x, point.y));
        }
    }
    const cv::Point *firstPoint = { &contourPoints[0] };
    const int size = contourPoints.size();
    cv::fillPoly(img_niv, &firstPoint, &size, 1, cv::Scalar(coul));
}

// Colorie chaque arête d'une approximation polygonale par une couleur différente
void colorier_morceaux(const ContourPol& cp, cv::Mat img)
{
    int color = 1;
    for (const auto& p : cp.points) {
        img.at<int>(p.y, p.x) = color;
        if (p.sommet) {
            img.at<int>(p.y, p.x) = color++;
            if (color == 255) color ++;
        }
    }
}

// Effecue l'approximation polygonale + le coloriage des contours d'une image
void approximer_et_colorier_contours_c8(const std::vector<ContourF8>& cfs, float seuil, cv::Mat img)
{
    for (const auto& cf : cfs) {
        auto cp = approximer_contour_c8(cf, seuil);
        colorier_morceaux(cp, img);
    }
}

// Transforme une image en son approximation polygonale (noir et blanc)
void approximer_et_remplir_contours_c8(std::vector<ContourF8> contours, float seuil, cv::Mat img)
{
    img.setTo(0);
    for(ContourF8 contour : contours)
    {
        auto cp = approximer_contour_c8(contour, seuil);
        if(contour.dir == 6 || contour.dir == 4) {
            remplir_poly(cp, img, 255);
        }
        else {
            remplir_poly(cp, img, 0);
        }
    }
}

// Effecture le pelage des formes contour par contour
void effectuer_pelage_DT(cv::Mat img_niv, bool connexite)
{

    std::function<void(int, int, const std::function<void(int,int)>&)> foreach_c =
            (connexite) ? foreach_c8 : foreach_c4;

    for(int y = 0; y < img_niv.rows; y++)
    {
        for(int x = 0; x < img_niv.cols; x++)
        {
            if(img_niv.at<int>(y, x) != 0) img_niv.at<int>(y, x) = INT_MAX;
        }
    }

    int k = 1;
    bool a_change;
    do {
        a_change = false;
        for(int y = 0; y < img_niv.rows; y++)
        {
            for(int x = 0; x < img_niv.cols; x++)
            {
                int& current_value = img_niv.at<int>(y, x);
                if(current_value < k) continue;
                foreach_c(y, x, [&](int row, int col)
                {
                    if(!is_valid(img_niv, row, col) || img_niv.at<int>(row, col) < k){
                        current_value = k;
                        a_change = true;
                    }
                });
            }
        }
        k ++;
    } while(a_change);
}

// Détecte les maximums locaux dans img_niv et crée une nouvelle image en conséquence
void detecter_maximums_locaux (cv::Mat img_niv, bool connexite)
{
    std::function<void(int, int, const std::function<void(int,int)>&)> foreach_c =
            (connexite) ? foreach_c8 : foreach_c4;

    cv::Mat tmp = img_niv.clone();

    for(int y = 0; y < img_niv.rows; y++)
    {
        for(int x = 0; x < img_niv.cols; x++)
        {
            int k = img_niv.at<int>(y, x);
            if(k > 0)
            {
                foreach_c(y, x, [&](int row, int col)
                {
                    if(img_niv.at<int>(row, col) > k) tmp.at<int>(y, x) = 0;
                });
            }
        }
    }

    for(int y = 0; y < img_niv.rows; y++)
    {
        for(int x = 0; x < img_niv.cols; x++)
        {
            img_niv.at<int>(y, x) = tmp.at<int>(y, x) ;
        }
    }
}

// Effectue la transformation de distance inverse par pelage
// à utiliser sur le résultat de detecter_maximums_locaux()
void effectuer_pelage_RDT (cv::Mat img_niv, int connexite)
{
    std::function<void(int, int, const std::function<void(int,int)>&)> foreach_c =
            (connexite) ? foreach_c8 : foreach_c4;

    int max = 0;
    for(int y = 0; y < img_niv.rows; y++)
    {
        for(int x = 0; x < img_niv.cols; x++)
        {
            int k = img_niv.at<int>(y, x);
            if(k > max) max = k;
        }
    }

    int k = max-1;
    while(k > 0)
    {
        for(int y = 0; y < img_niv.rows; y++)
        {
            for(int x = 0; x < img_niv.cols; x++)
            {
                int& value = img_niv.at<int>(y, x);
                if(value >= k) continue;
                foreach_c(y, x, [&](int row, int col)
                {
                    if(!is_valid(img_niv, row, col)) return;
                    if(img_niv.at<int>(row, col) > k) value = k;
                });
            }
        }
        k--;
    }
}

// Appelez ici vos transformations selon affi
void effectuer_transformations (My::Affi affi, cv::Mat img_niv)
{
    switch (affi) {
        case My::A_TRANS1 :
            marquer_contours_c8 (img_niv);
            break;
        case My::A_TRANS2 :
            marquer_contours_c4 (img_niv);
            break;
        case My::A_TRANS3 :
            numeroter_contours_c8 (img_niv);
            break;
        case My::A_TRANS4 :
            effectuer_suivi_contours_c8 (img_niv);
            break;
        case My::A_TRANS5 :
            approximer_et_colorier_contours_c8(effectuer_suivi_contours_c8 (img_niv), seuil_Polyg/100.f, img_niv);
            break;
        case My::A_TRANS6 :
            approximer_et_remplir_contours_c8(effectuer_suivi_contours_c8 (img_niv), seuil_Polyg/100.f, img_niv);
            break;
        case My::A_TRANS7 :
            approximer_et_remplir_contours_c8(effectuer_suivi_contours_c8 (img_niv), seuil_Polyg/100.f, img_niv);
            effectuer_pelage_DT(img_niv, connexite);
            break;
        case My::A_TRANS8 :
            approximer_et_remplir_contours_c8(effectuer_suivi_contours_c8 (img_niv), seuil_Polyg/100.f, img_niv);
            effectuer_pelage_DT(img_niv, connexite);
            detecter_maximums_locaux(img_niv, connexite);
            break;
        case My::A_TRANS9 :
            approximer_et_remplir_contours_c8(effectuer_suivi_contours_c8 (img_niv), seuil_Polyg/100.f, img_niv);
            effectuer_pelage_DT(img_niv, connexite);
            detecter_maximums_locaux(img_niv, connexite);
            effectuer_pelage_RDT(img_niv, connexite);
            break;
        default :
            break;
    }
}

//---------------------------- C A L L B A C K S ------------------------------

// Callback des sliders
void onZoomSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->loupe.reborner (my->img_res1, my->img_res2);
    my->set_recalc(My::R_LOUPE);
}

void onSeuilSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->set_recalc(My::R_SEUIL);
}

// Callback pour la souris
void onMouseEventSrc (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->clic_x = x;
            my->clic_y = y;
            my->clic_n = 1;
            break;
        case cv::EVENT_MOUSEMOVE :
            // std::cout << "mouse move " << x << "," << y << std::endl;
            if (my->clic_n == 1) {
                my->loupe.deplacer (my->img_res1, my->img_res2, 
                    x - my->clic_x, y - my->clic_y);
                my->clic_x = x;
                my->clic_y = y;
                my->set_recalc(My::R_LOUPE);
            }
            break;
        case cv::EVENT_LBUTTONUP :
            my->clic_n = 0;
            break;
    }
}


void onMouseEventLoupe (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->loupe.afficher_tableau_valeurs (my->img_niv, x, y, 5, 4);
            break;
    }
}


void afficher_aide() {
    // Indiquez les transformations ici
    std::cout <<
        "Touches du clavier:\n"
        "   a    affiche cette aide\n"
        " hHlL   change la taille de la loupe\n"
        "   i    inverse les couleurs de src\n"
        "   o    affiche l'image src originale\n"
        "   s    affiche l'image src seuillée\n"
        "   1    contours pour c8\n"
        "   2    contours pour c4\n"
        "   3    numérotation des contours pour c8\n"
        "   4    suivi de contour pour c8\n"
        "   5    approximation polygonale + coloriage\n"
        "   6    approximation polygonale + remplissage\n"
        "   c    change la connexité pour les traitements suivants :\n"
        "   7    DT par pelage\n"
        "   8    maximums locaux\n"
        "   9    RDT par pelage\n"
        "  esc   quitte\n"
    << std::endl;
}

// Callback "maison" pour le clavier
int onKeyPressEvent (int key, void *data)
{
    My *my = (My*) data;

    if (key < 0) return 0;        // aucune touche pressée
    key &= 255;                   // pour comparer avec un char
    if (key == 27) return -1;     // ESC pour quitter

    switch (key) {
        case 'a' :
            afficher_aide();
            break;
        case 'h' :
        case 'H' :
        case 'l' :
        case 'L' : {
            std::cout << "Taille loupe" << std::endl;
            int h = my->img_res2.rows, w = my->img_res2.cols; 
            if      (key == 'h') h = h >=  200+100 ? h-100 :  200;
            else if (key == 'H') h = h <= 2000-100 ? h+100 : 2000;
            else if (key == 'l') w = w >=  200+100 ? w-100 :  200;
            else if (key == 'L') w = w <= 2000-100 ? w+100 : 2000;
            my->img_res2 = cv::Mat(h, w, CV_8UC3);
            my->loupe.reborner(my->img_res1, my->img_res2);
            my->set_recalc(My::R_LOUPE);
          } break;
        case 'i' :
            std::cout << "Couleurs inversées" << std::endl;
            inverser_couleurs(my->img_src);
            my->set_recalc(My::R_SEUIL);
            break;
        case 'o' :
            std::cout << "Image originale" << std::endl;
            my->affi = My::A_ORIG;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case 's' :
            std::cout << "Image seuillée" << std::endl;
            my->affi = My::A_SEUIL;
            my->set_recalc(My::R_SEUIL);
            break;

        // Rajoutez ici des touches pour les transformations.
        // Dans my->set_recalc, passez :
        //   My::R_SEUIL pour faire le calcul à partir de l'image originale seuillée
        //   My::R_TRANSFOS pour faire le calcul à partir de l'image actuelle
        case '1' :
            std::cout << "Contour c8" << std::endl;
            my->affi = My::A_TRANS1;
            my->set_recalc(My::R_SEUIL);
            break;
        case '2' :
            std::cout << "Contour C4" << std::endl;
            my->affi = My::A_TRANS2;
            my->set_recalc(My::R_SEUIL);
            break;
        case '3' :
            std::cout << "Numérotation des contours C8" << std::endl;
            my->affi = My::A_TRANS3;
            my->set_recalc(My::R_SEUIL);
            break;
        case '4' :
            std::cout << "Suivi de contour pour C8" << std::endl;
            my->affi = My::A_TRANS4;
            my->set_recalc(My::R_SEUIL);
            break;
        case '5' :
            std::cout << "Approximation polynomiale + coloriage" << std::endl;
            my->affi = My::A_TRANS5;
            my->set_recalc(My::R_SEUIL);
            break;
        case '6' :
            std::cout << "Approximation polynomiale + remplissage" << std::endl;
            my->affi = My::A_TRANS6;
            my->set_recalc(My::R_SEUIL);
            break;
        case '7' :
            std::cout << "DT par pelage" << std::endl;
            my->affi = My::A_TRANS7;
            my->set_recalc(My::R_SEUIL);
            break;
        case '8' :
            std::cout << "Maximums locaux" << std::endl;
            my->affi = My::A_TRANS8;
            my->set_recalc(My::R_SEUIL);
            break;
        case '9' :
            std::cout << "DT par pelage -> Maximums locaux -> RDT par pelage" << std::endl;
            my->affi = My::A_TRANS9;
            my->set_recalc(My::R_SEUIL);
            break;
        case 'c' :
            if(my->affi != My::A_TRANS7 && my->affi != My::A_TRANS8 && my->affi != My::A_TRANS9) break;
            std::cout << "Changement de connexité" << std::endl;
            connexite = !connexite;
            my->set_recalc(My::R_SEUIL);
            break;
        default :
            break;
    }
    return 1;
}


//---------------------------------- M A I N ----------------------------------

void afficher_usage (char *nom_prog) {
    std::cout << "Usage: " << nom_prog
              << "[-mag width height] [-thr seuil] in1 [out2]" 
              << std::endl;
}

int main (int argc, char**argv)
{
    My my;
    char *nom_in1, *nom_out2, *nom_prog = argv[0];
    int zoom_w = 600, zoom_h = 500;

    while (argc-1 > 0) {
        if (!strcmp(argv[1], "-mag")) {
            if (argc-1 < 3) { afficher_usage(nom_prog); return 1; }
            zoom_w = atoi(argv[2]);
            zoom_h = atoi(argv[3]);
            argc -= 3; argv += 3;
        } else if (!strcmp(argv[1], "-thr")) {
            if (argc-1 < 2) { afficher_usage(nom_prog); return 1; }
            my.seuil = atoi(argv[2]);
            argc -= 2; argv += 2;
        } else break;
    }
    if (argc-1 < 1 or argc-1 > 2) { afficher_usage(nom_prog); return 1; }
    nom_in1  = argv[1];
    nom_out2 = (argc-1 == 2) ? argv[2] : NULL;

    // Lecture image
    my.img_src = cv::imread (nom_in1, cv::IMREAD_COLOR);  // produit du 8UC3
    if (my.img_src.empty()) {
        std::cout << "Erreur de lecture" << std::endl;
        return 1;
    }

    // Création résultats
    my.img_res1 = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.img_res2 = cv::Mat(zoom_h, zoom_w, CV_8UC3);
    my.img_niv  = cv::Mat(my.img_src.rows, my.img_src.cols, CV_32SC1);
    my.img_coul = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.loupe.reborner(my.img_res1, my.img_res2);

    // Création fenêtre
    cv::namedWindow ("ImageSrc", cv::WINDOW_AUTOSIZE);
    cv::createTrackbar ("Zoom", "ImageSrc", &my.loupe.zoom, my.loupe.zoom_max, 
        onZoomSlide, &my);
    cv::createTrackbar ("Seuil", "ImageSrc", &my.seuil, 255, 
        onSeuilSlide, &my);
    cv::createTrackbar ("Polyg", "ImageSrc", &seuil_Polyg, 1000,
        onSeuilSlide, &my);
    cv::setMouseCallback ("ImageSrc", onMouseEventSrc, &my);

    cv::namedWindow ("Loupe", cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback ("Loupe", onMouseEventLoupe, &my);

    afficher_aide();

    // Boucle d'événements
    for (;;) {

        if (my.need_recalc(My::R_SEUIL)) 
        {
            // std::cout << "Calcul seuil" << std::endl;
            cv::Mat img_gry;
            cv::cvtColor (my.img_src, img_gry, cv::COLOR_BGR2GRAY);
            cv::threshold (img_gry, img_gry, my.seuil, 255, cv::THRESH_BINARY);
            img_gry.convertTo (my.img_niv, CV_32SC1,1., 0.);
        }

        if (my.need_recalc(My::R_TRANSFOS))
        {
            // std::cout << "Calcul transfos" << std::endl;
            if (my.affi != My::A_ORIG) {
                effectuer_transformations (my.affi, my.img_niv);
                representer_en_couleurs_vga (my.img_niv, my.img_coul);
            } else my.img_coul = my.img_src.clone();
        }

        if (my.need_recalc(My::R_LOUPE)) {
            // std::cout << "Calcul loupe puis affichage" << std::endl;
            my.loupe.dessiner_rect    (my.img_coul, my.img_res1);
            my.loupe.dessiner_portion (my.img_coul, my.img_res2);
            cv::imshow ("ImageSrc", my.img_res1);
            cv::imshow ("Loupe"   , my.img_res2);
        }
        my.reset_recalc();

        // Attente du prochain événement sur toutes les fenêtres, avec un
        // timeout de 15ms pour détecter les changements de flags
        int key = cv::waitKey (15);

        // Gestion des événements clavier avec une callback "maison" que l'on
        // appelle nous-même. Les Callbacks souris et slider sont directement
        // appelées par waitKey lors de l'attente.
        if (onKeyPressEvent (key, &my) < 0) break;
    }

    // Enregistrement résultat
    if (nom_out2) {
        if (! cv::imwrite (nom_out2, my.img_coul))
             std::cout << "Erreur d'enregistrement" << std::endl;
        else std::cout << "Enregistrement effectué" << std::endl;
     }
    return 0;
}

