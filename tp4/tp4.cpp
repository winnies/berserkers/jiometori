/*
    Exemples de transformations en OpenCV, avec zoom, seuil et affichage en
    couleurs. L'image de niveau est en CV_32SC1.

    $ make ex01-transfos
    $ ./ex01-transfos [-mag width height] [-thr seuil] image_in [image_out]

    CC BY-SA Edouard.Thiel@univ-amu.fr - 07/09/2020

                        --------------------------------

    Renommez ce fichier tp<n°>-<vos-noms>.cpp
    Écrivez ci-dessous vos NOMS Prénoms et la date de la version :

    Escales Loïc - Galland Thomas - version du 27/11/2020
*/

#include <iostream>
#include <iomanip>
#include <cstring>
#include <opencv2/opencv.hpp>
#include "gd-util.hpp"
#include <queue>

//----------------------------------- M Y -------------------------------------

// Retourne vrai si les coordonnées correspondent à un pixel de l'image (matrice)
bool is_valid(const cv::Mat& img, int row, int col) {
    return 0 <= row && row < img.rows && 0 <= col && col < img.cols;
}

void calculer_sedt_saito_toriwaki (cv::Mat img_niv)
{
    // Premier passage (lignes)
    for (int y = 0; y < img_niv.rows; y++)
    {
        int dist = 0;
        for (int x = 0; x < img_niv.cols; x++)
        {
            int &pix = img_niv.at<int>(y, x);
            if(pix == 0) dist = 0;
            else pix = std::pow(++dist, 2);
        }
        dist = 0;
        for (int x = img_niv.cols - 1; x >= 0; x--)
        {
            int &pix = img_niv.at<int>(y, x);
            if(pix == 0) dist = 0;
            else pix = std::min<int>(std::pow(++dist, 2), pix);
        }
    }

    // Deuxième passage (colonnes)
    for(int x = 0; x < img_niv.cols; x++)
    {
        std::vector<int> col_copy;
        for(int y = 0; y < img_niv.rows; y++)
        {
            col_copy.push_back(img_niv.at<int>(y, x));
        }
        for(int y = 0; y < img_niv.rows; y++)
        {
            int &pix = img_niv.at<int>(y, x);
            if(pix == 0) continue;
            int min_val = pix;
            for(int y2 = y-1; y2 >= 0; y2--)
            {
                int dist = std::pow(y - y2, 2);
                if(dist >= min_val) break;
                min_val = std::min<int>(col_copy[y2] + dist, min_val);
            }
            min_val = std::min<int>(std::pow(y+1, 2), min_val);
            for(int y2 = y+1; y2 < img_niv.rows; y2++)
            {
                int dist = std::pow(y - y2, 2);
                if(dist >= min_val) break;
                min_val = std::min<int>(col_copy[y2] + dist, min_val);
            }
            min_val = std::min<int>(std::pow(y - img_niv.rows, 2), min_val);
            pix = min_val;
        }
    }
}

void calculer_sedt_courbes_niveau (cv::Mat img_niv)
{
    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        int& pix = img_niv.at<int>(y, x);
        pix = std::sqrt(pix);
    }
}

void foreach_c8(int row, int col, const std::function<void(int,int)>& op) {
    for (int i = -1; i < 2; ++i) {
        for (int j = -1; j < 2; ++j) {
            int r = row + i, c = col + j;
            if (r == row && c == col) continue;
            op(r, c);
        }
    }
}

static const int foreach_c8_cw_coords[8][2] = {{-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}};
void foreach_c8_cw(int row, int col, const std::function<void(int,int)>& op) {
    for (int const* coord : foreach_c8_cw_coords) {
        int r = row + coord[0], c = col + coord[1];
        op(r, c);
    }
}

bool is_simple_c8(cv::Mat img_src, int x, int y) {
    cv::Mat img_local = cv::Mat::zeros(3, 3, CV_32SC1);
    for (int yy = 0; yy < img_local.rows; yy++)
    for (int xx = 0; xx < img_local.cols; xx++)
    {
        int nx = xx - 1 + x; int ny = yy - 1 + y;
        if (!is_valid(img_src, ny, nx)) continue;
        img_local.at<int>(yy, xx) = img_src.at<int>(ny, nx);
    }
    img_local.at<int>(1, 1) = 0;

    float n_vertices = 0, n_edges = 0, n_angles = 0;

    foreach_c8(1, 1, [&](int r, int c) {
        if (!is_valid(img_local, r, c) || img_local.at<int>(r, c) <= 0) return;
        n_vertices += 1;

        int new_edges = 0;
        foreach_c8(r, c, [&](int rr, int cc) {
            if (!is_valid(img_local, rr, cc) || img_local.at<int>(rr, cc) <= 0) return;
            new_edges += 1;
        });

        n_edges += new_edges;

        if (new_edges == 2 && r != 1 && c != 1) {
            n_angles += 1;
        }
    });

    return std::round(n_vertices - (n_edges / 2.f - n_angles)) == 1.f;
}

struct Pixel {
    int x, y;

    bool operator==(const Pixel& p) const {
        return x == p.x && y == p.y;
    }
};

int& at(cv::Mat& img, const Pixel& p) {
    return img.at<int>(p.y, p.x);
}

bool is_end_point(const cv::Mat& img_niv, const Pixel& p) {
    int ny = p.y + foreach_c8_cw_coords[7][0], nx = p.x + foreach_c8_cw_coords[7][1];
    bool is_prec_pixel_valid = is_valid(img_niv, ny, nx) && img_niv.at<int>(ny, nx) > 0;

    int n_valid_pixels = 0;
    bool consecutive_valid_pixels_found = false;

    foreach_c8_cw(p.y, p.x, [&](int r, int c) {
        if (is_valid(img_niv, r, c) && img_niv.at<int>(r, c) > 0) {
            n_valid_pixels += 1;
            consecutive_valid_pixels_found = consecutive_valid_pixels_found || is_prec_pixel_valid;
            is_prec_pixel_valid = true;
        } else {
            is_prec_pixel_valid = false;
        }
    });

    return (n_valid_pixels == 1) || (n_valid_pixels == 2 && consecutive_valid_pixels_found);
}

/* la forme doit être en blanc et le fond en noir */
void skeleton(cv::Mat img_niv) {
    cv::Mat img_dist = img_niv.clone();
    calculer_sedt_saito_toriwaki(img_dist);
    calculer_sedt_courbes_niveau(img_dist);

    cv::Mat img_end_point = cv::Mat::zeros(img_niv.rows, img_niv.cols, CV_32SC1);
    for (int yy = 0; yy < img_end_point.rows; yy++)
    for (int xx = 0; xx < img_end_point.cols; xx++)
    img_end_point.at<int>(yy, xx);

    auto pixels_comp = [&](const Pixel& p1, const Pixel& p2) {
        return at(img_dist, p1) > at(img_dist, p2);
    };
    std::priority_queue<Pixel, std::vector<Pixel>, decltype (pixels_comp)> pixels(pixels_comp);

    for (int y = 0; y < img_niv.rows; y++)
    for (int x = 0; x < img_niv.cols; x++)
    {
        if(img_dist.at<int>(y, x) == 1 && is_simple_c8(img_niv, x, y)) {
            pixels.push(Pixel{x, y});
        }
    }

    while (!pixels.empty()) {
        Pixel p = pixels.top();
        pixels.pop();

        if (is_simple_c8(img_niv, p.x, p.y) && !(is_end_point(img_niv, p) || at(img_end_point, p))) {
            at(img_niv, p) = 0;

            foreach_c8(p.y, p.x, [&](int r, int c) {
                if (!is_valid(img_niv, r, c)) return;
                if (is_simple_c8(img_niv, c, r)) {
                    pixels.push(Pixel{c, r});
                } else {
                    at(img_end_point, p) = 1;
                }
            });
        }
    }
}

class My {
  public:
    cv::Mat img_src, img_res1, img_res2, img_niv, img_coul;
    Loupe loupe;
    int seuil = 127;
    int clic_x = 0;
    int clic_y = 0;
    int clic_n = 0;

    enum Recalc { R_RIEN, R_LOUPE, R_TRANSFOS, R_SEUIL };
    Recalc recalc = R_SEUIL;

    void reset_recalc ()             { recalc = R_RIEN; }
    void set_recalc   (Recalc level) { if (level > recalc) recalc = level; }
    int  need_recalc  (Recalc level) { return level <= recalc; }

    // Rajoutez ici des codes A_TRANSx pour le calcul et l'affichage
    enum Affi { A_ORIG, A_SEUIL, A_TRANS1, A_TRANS2, A_TRANS3, A_TRANS4, A_TRANS5, A_TRANS6, A_TRANS7 };
    Affi affi = A_ORIG;
};

//----------------------- T R A N S F O R M A T I O N S -----------------------

// Appelez ici vos transformations selon affi
void effectuer_transformations (My::Affi affi, cv::Mat img_niv)
{
    switch (affi) {
        case My::A_TRANS1 :
            calculer_sedt_saito_toriwaki(img_niv);
            calculer_sedt_courbes_niveau(img_niv);
            break;
        case My::A_TRANS2 :
            skeleton(img_niv);
            break;
        default : ;
    }
}


//---------------------------- C A L L B A C K S ------------------------------

// Callback des sliders
void onZoomSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->loupe.reborner (my->img_res1, my->img_res2);
    my->set_recalc(My::R_LOUPE);
}

void onSeuilSlide (int pos, void *data)
{
    My *my = (My*) data;
    my->set_recalc(My::R_SEUIL);
}


// Callback pour la souris
void onMouseEventSrc (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->clic_x = x;
            my->clic_y = y;
            my->clic_n = 1;
            break;
        case cv::EVENT_MOUSEMOVE :
            // std::cout << "mouse move " << x << "," << y << std::endl;
            if (my->clic_n == 1) {
                my->loupe.deplacer (my->img_res1, my->img_res2,
                    x - my->clic_x, y - my->clic_y);
                my->clic_x = x;
                my->clic_y = y;
                my->set_recalc(My::R_LOUPE);
            }
            break;
        case cv::EVENT_LBUTTONUP :
            my->clic_n = 0;
            break;
    }
}


void onMouseEventLoupe (int event, int x, int y, int flags, void *data)
{
    My *my = (My*) data;

    switch (event) {
        case cv::EVENT_LBUTTONDOWN :
            my->loupe.afficher_tableau_valeurs (my->img_niv, x, y, 5, 4);
            break;
    }
}


void afficher_aide() {
    // Indiquez les transformations ici
    std::cout <<
        "Touches du clavier:\n"
        "   a    affiche cette aide\n"
        " hHlL   change la taille de la loupe\n"
        "   i    inverse les couleurs de src\n"
        "   o    affiche l'image src originale\n"
        "   s    affiche l'image src seuillée\n"
        "   1    affiche les courbes de niveau à partir du SEDT\n"
        "   2    affiche la squelettisation 2D\n"
        "  esc   quitte\n"
    << std::endl;
}

// Callback "maison" pour le clavier
int onKeyPressEvent (int key, void *data)
{
    My *my = (My*) data;

    if (key < 0) return 0;        // aucune touche pressée
    key &= 255;                   // pour comparer avec un char
    if (key == 27) return -1;     // ESC pour quitter

    switch (key) {
        case 'a' :
            afficher_aide();
            break;
        case 'h' :
        case 'H' :
        case 'l' :
        case 'L' : {
            std::cout << "Taille loupe" << std::endl;
            int h = my->img_res2.rows, w = my->img_res2.cols;
            if      (key == 'h') h = h >=  200+100 ? h-100 :  200;
            else if (key == 'H') h = h <= 2000-100 ? h+100 : 2000;
            else if (key == 'l') w = w >=  200+100 ? w-100 :  200;
            else if (key == 'L') w = w <= 2000-100 ? w+100 : 2000;
            my->img_res2 = cv::Mat(h, w, CV_8UC3);
            my->loupe.reborner(my->img_res1, my->img_res2);
            my->set_recalc(My::R_LOUPE);
          } break;
        case 'i' :
            std::cout << "Couleurs inversées" << std::endl;
            inverser_couleurs(my->img_src);
            my->set_recalc(My::R_SEUIL);
            break;
        case 'o' :
            std::cout << "Image originale" << std::endl;
            my->affi = My::A_ORIG;
            my->set_recalc(My::R_TRANSFOS);
            break;
        case 's' :
            std::cout << "Image seuillée" << std::endl;
            my->affi = My::A_SEUIL;
            my->set_recalc(My::R_SEUIL);
            break;

        // Rajoutez ici des touches pour les transformations.
        // Dans my->set_recalc, passez :
        //   My::R_SEUIL pour faire le calcul à partir de l'image originale seuillée
        //   My::R_TRANSFOS pour faire le calcul à partir de l'image actuelle
        case '1' :
            std::cout << "SEDT de Saito et Toriwaki" << std::endl;
            my->affi = My::A_TRANS1;
            my->set_recalc(My::R_SEUIL);
            break;
        case '2' :
            std::cout << "Squelettisation 2D" << std::endl;
            my->affi = My::A_TRANS2;
            my->set_recalc(My::R_SEUIL);
            break;
        default :
            //std::cout << "Touche '" << char(key) << "'" << std::endl;
            break;
    }
    return 1;
}


//---------------------------------- M A I N ----------------------------------

void afficher_usage (char *nom_prog) {
    std::cout << "Usage: " << nom_prog
              << "[-mag width height] [-thr seuil] in1 [out2]"
              << std::endl;
}

int main (int argc, char**argv)
{
    My my;
    char *nom_in1, *nom_out2, *nom_prog = argv[0];
    int zoom_w = 600, zoom_h = 500;

    while (argc-1 > 0) {
        if (!strcmp(argv[1], "-mag")) {
            if (argc-1 < 3) { afficher_usage(nom_prog); return 1; }
            zoom_w = atoi(argv[2]);
            zoom_h = atoi(argv[3]);
            argc -= 3; argv += 3;
        } else if (!strcmp(argv[1], "-thr")) {
            if (argc-1 < 2) { afficher_usage(nom_prog); return 1; }
            my.seuil = atoi(argv[2]);
            argc -= 2; argv += 2;
        } else break;
    }
    if (argc-1 < 1 or argc-1 > 2) { afficher_usage(nom_prog); return 1; }
    nom_in1  = argv[1];
    nom_out2 = (argc-1 == 2) ? argv[2] : NULL;

    // Lecture image
    my.img_src = cv::imread (nom_in1, cv::IMREAD_COLOR);  // produit du 8UC3
    if (my.img_src.empty()) {
        std::cout << "Erreur de lecture" << std::endl;
        return 1;
    }

    // Création résultats
    my.img_res1 = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.img_res2 = cv::Mat(zoom_h, zoom_w, CV_8UC3);
    my.img_niv  = cv::Mat(my.img_src.rows, my.img_src.cols, CV_32SC1);
    my.img_coul = cv::Mat(my.img_src.rows, my.img_src.cols, CV_8UC3);
    my.loupe.reborner(my.img_res1, my.img_res2);

    // Création fenêtre
    cv::namedWindow ("ImageSrc", cv::WINDOW_AUTOSIZE);
    cv::createTrackbar ("Zoom", "ImageSrc", &my.loupe.zoom, my.loupe.zoom_max,
        onZoomSlide, &my);
    cv::createTrackbar ("Seuil", "ImageSrc", &my.seuil, 255,
        onSeuilSlide, &my);
    cv::setMouseCallback ("ImageSrc", onMouseEventSrc, &my);

    cv::namedWindow ("Loupe", cv::WINDOW_AUTOSIZE);
    cv::setMouseCallback ("Loupe", onMouseEventLoupe, &my);

    afficher_aide();

    // Boucle d'événements
    for (;;) {

        if (my.need_recalc(My::R_SEUIL))
        {
            // std::cout << "Calcul seuil" << std::endl;
            cv::Mat img_gry;
            cv::cvtColor (my.img_src, img_gry, cv::COLOR_BGR2GRAY);
            cv::threshold (img_gry, img_gry, my.seuil, 255, cv::THRESH_BINARY);
            img_gry.convertTo (my.img_niv, CV_32SC1,1., 0.);
        }

        if (my.need_recalc(My::R_TRANSFOS))
        {
            // std::cout << "Calcul transfos" << std::endl;
            if (my.affi != My::A_ORIG) {
                effectuer_transformations (my.affi, my.img_niv);
                representer_en_couleurs_vga (my.img_niv, my.img_coul);
            } else my.img_coul = my.img_src.clone();
        }

        if (my.need_recalc(My::R_LOUPE)) {
            // std::cout << "Calcul loupe puis affichage" << std::endl;
            my.loupe.dessiner_rect    (my.img_coul, my.img_res1);
            my.loupe.dessiner_portion (my.img_coul, my.img_res2);
            cv::imshow ("ImageSrc", my.img_res1);
            cv::imshow ("Loupe"   , my.img_res2);
        }
        my.reset_recalc();

        // Attente du prochain événement sur toutes les fenêtres, avec un
        // timeout de 15ms pour détecter les changements de flags
        int key = cv::waitKey (15);

        // Gestion des événements clavier avec une callback "maison" que l'on
        // appelle nous-même. Les Callbacks souris et slider sont directement
        // appelées par waitKey lors de l'attente.
        if (onKeyPressEvent (key, &my) < 0) break;
    }

    // Enregistrement résultat
    if (nom_out2) {
        if (! cv::imwrite (nom_out2, my.img_coul))
             std::cout << "Erreur d'enregistrement" << std::endl;
        else std::cout << "Enregistrement effectué" << std::endl;
     }
    return 0;
}


